# Author: Rizky Ramadhan

# Libraries
# pip install docx2txt
# pip install Flask
# pip install Werkzeug
# pip install nltk

import docx2txt
import os
from flask import Flask, render_template, request, redirect
from werkzeug.utils import secure_filename
from nltk.corpus import stopwords
from string import punctuation
from summarization import get_clean_text, get_removed_stopword, get_word_frequency, get_dict_of_clean_sentences, get_sentence_frequency
from nltk.tokenize import word_tokenize
from collections import Counter
from heapq import nlargest

app = Flask(__name__, static_url_path="", static_folder="static")

app.config['UPLOAD_FOLDER'] = 'uploads'

ALLOWED_EXTENSION = set(["docx"])


# Return true jika format file tersedia dalam ALLOWED_EXTENSION

def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSION


# Route untuk halaman depan

@ app.route('/', methods=["GET", "POST"])
def home():
    title = "Auto Summarization"
    return render_template("index.html", title=title)


# Route untuk halaman about

@ app.route('/about')
def about():
    title = "About"
    return render_template("about.html", title=title)


# Route untuk menampilkan hasil dari rangkuman

@app.route("/summarization", methods=["GET", "POST"])
def summarization():
    title = "Auto Summarization"

    if request.method == "POST":

        # Data dari form

        document = request.files["document"]
        summary_length = request.form.get("summaryLength")

        # Kembali ke halaman depan jika document tidak ada

        if "document" not in request.files:
            return render_template("index.html", title=title)

        # Kembali ke halaman depan jika nama document kosong

        if document.filename == "":
            return render_template("index.html", title=title)

        # Jika dokument sesuai dengan file yang diperbolehkan

        if document and allowed_file(document.filename):
            title = "Summarization Result"

            # Simpan dokument di dalam folder uploads

            filename = secure_filename(document.filename)
            document.save(os.path.join(app.config['UPLOAD_FOLDER'],  filename))

            # Membaca document yang sudah disimpan

            document_result = docx2txt.process(f"./uploads/{filename}")

            # Teks yang akan dirangkum

            news_text = "".join(document_result.split("\n"))
            news_text_sentences = document_result.split("\n")

            # Stopwords Bahasa Indonesia dan tanda baca atau punctuations.

            list_of_stopword = list(set(stopwords.words('indonesian')))
            punctuations = punctuation + "\n“”"

            # Menghapus semua tanda baca pada teks

            clean_news_text = get_clean_text(news_text, punctuations)

            # Mengubah teks menjadi token

            clean_news_text_token = word_tokenize(clean_news_text)

            # Menghapus stop words

            removed_stopword_news_token = get_removed_stopword(
                clean_news_text_token, list_of_stopword)

            # Menghitung jumlah kemunculan setiap token

            most_common_news_token = Counter(removed_stopword_news_token)

            # Mendapatkan nilai tertinggi dari kemunculan token

            max_frequency_token = most_common_news_token.most_common(1)[0][1]

            # Menghitung frequency setiap token

            news_token_frequency = get_word_frequency(
                most_common_news_token, max_frequency_token)

            # Memberikan nilai frquency awal 0.0 untuk setiap paragraf

            clean_news_text_sentences = get_dict_of_clean_sentences(
                news_text_sentences, punctuations)

            # Menghitung frequency setiap paragraf

            news_sentences_frequency = get_sentence_frequency(
                clean_news_text_sentences, news_token_frequency)

            # Mendapatkan nilai ringkasan dari teks

            select_length = int(len(news_sentences_frequency)
                                * float(summary_length))

            # Hasil dari ringkasan

            summary = nlargest(
                select_length, news_sentences_frequency, key=news_sentences_frequency.get)

            # Menghitung panjang dari setiap paragraf

            real_document_length = len(news_sentences_frequency)
            summary_document_length = len(summary)

            return render_template("summarization.html", title=title, real_document=news_text_sentences, document_result=summary, real_document_length=real_document_length, summary_document_length=summary_document_length)

        return render_template("index.html", title=title)

    else:
        return render_template("index.html", title=title)


if __name__ == "__main__":
    app.run(debug=True)
