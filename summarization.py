# Return clean text, menghapus punctuations.


def get_clean_text(text, punctuations):
    return text.translate(str.maketrans("", "", punctuations)).lower()


# Return token tanpa stop words.

def get_removed_stopword(list_of_token, list_of_stopword):
    return [token for token in list_of_token if token not in list_of_stopword]


# Return frequency dari setiap token

def get_word_frequency(dict_of_token, max_frequency):
    dict_of_token_frequency = {}

    for token in dict_of_token:
        dict_of_token_frequency[token] = dict_of_token.get(
            token) / max_frequency

    return dict_of_token_frequency


# Return paragraf dengan nilai frequency awal 0.0

def get_dict_of_clean_sentences(list_of_sentences, punctuations):
    dict_of_clean_sentences = {}

    for sentences in list_of_sentences:
        if sentences:
            dict_of_clean_sentences[sentences] = 0.0

    return dict_of_clean_sentences


# Return paragraf yang memiliki nilai frequency

def get_sentence_frequency(dict_of_sentences, dict_of_token_frequency):
    dict_of_sentences_frequency = dict_of_sentences

    for sentences in dict_of_sentences.keys():
        for word in sentences.lower().split(" "):
            if word in dict_of_token_frequency.keys():
                dict_of_sentences_frequency[sentences] += dict_of_token_frequency.get(
                    word)

    return dict_of_sentences_frequency
